from django.db import models
from django.utils import timezone

class Message(models.Model):
	message = models.CharField(max_length=300)
	time = models.DateTimeField(auto_now_add=True, blank=True)