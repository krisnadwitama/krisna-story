import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *

class StoryAppUnitTest(TestCase):
	def test_story_app_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_story_app_using_index_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	def test_story_app_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_landing_page_content(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Hello, apa kabar?', html_response)

	def test_create_message_models(self):
		cnt_prev = Message.objects.all().count()
		Message.objects.create(message='Test')
		cnt = Message.objects.all().count()
		self.assertEqual(cnt, cnt_prev+1)

	def test_data_in_form_is_displayed(self):
		message = "Hai"
		response = Client().post('/', {'message': message})
		self.assertIn(message, response.content.decode())

	def test_about_me_url_is_exist(self):
		response = Client().get('/about_me')
		self.assertEqual(response.status_code, 200)

	def test_about_me_func(self):
		found = resolve('/about_me')
		self.assertEqual(found.func, about_me)

	def test_page_using_about_me_template(self):
		response = Client().get('/about_me')
		self.assertTemplateUsed(response, 'about_me.html')

	def test_about_me_page_content(self):
		request = HttpRequest()
		response = about_me(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Nama', html_response)
		self.assertIn('NPM', html_response)
		self.assertIn('I Made Krisna Dwitama', html_response)
		self.assertIn('1806133881', html_response)
		self.assertIn('<img', html_response)