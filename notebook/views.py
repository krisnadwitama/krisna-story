from django.shortcuts import render
from django.contrib.auth import logout
from django.http import HttpResponseRedirect

def notebook(request):
	response = render(request, 'notebook.html')
	if(request.user.is_authenticated):
		response.set_cookie('username', request.user.username)
		response.set_cookie('password', request.user.password)
		response.set_cookie('first_name', request.user.first_name)
	return response

def auth_logout(request):
	logout(request)
	request.session.flush()
	return HttpResponseRedirect('/')