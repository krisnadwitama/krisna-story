$(document).ready(function() {
 	$("#dark_button").click(function(){
 		localStorage.setItem("css", "/static/dark_theme.css");
 		themeSet();
 	});
 	$("#light_button").click(function(){
 		localStorage.setItem("css", "/static/light_theme.css");
 		themeSet();
 	});
 	$("#dark_button").mouseover(function(){
 		$("#css").attr("href", "/static/dark_theme.css");
 	});
 	$("#dark_button").mouseout(function(){
 		themeSet();
 	});
 	$("#light_button").mouseover(function(){
 		$("#css").attr("href", "/static/light_theme.css");
 	});
 	$("#light_button").mouseout(function(){
 		themeSet();
 	});
 	themeSet();
});

function themeSet(){
	if(typeof(Storage) !== "undefined"){
		var theme = localStorage.getItem("css");
		if(theme === null){
			$("#css").attr("href", "/static/light_theme.css");
		}
		else{
			$("#css").attr("href", theme);
		}
	}
	else {
		$("#css").attr("href", "/static/light_theme.css");
	}
}